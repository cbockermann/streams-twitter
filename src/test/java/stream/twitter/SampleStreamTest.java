/**
 * 
 */
package stream.twitter;

import static org.junit.Assert.fail;

import java.net.URL;

import org.junit.Test;

/**
 * @author chris
 * 
 */
public class SampleStreamTest {

	@Test
	public void test() {
	}

	public static void main(String[] args) {
		try {
			URL url = SampleStreamTest.class.getResource("/soccer-stream.xml");
			stream.run.main(url);
		} catch (Exception e) {
			fail("Error: " + e.getMessage());
		}
	}
}
