/**
 * 
 */
package stream.twitter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractTwitterStream;

/**
 * @author chris
 * 
 */
public class SampleStream extends AbstractTwitterStream {

	static Logger log = LoggerFactory.getLogger(SampleStream.class);

	public SampleStream() {
	}

	public SampleStream(URL url) {
		this.twitterUrl = url.toString();
	}

	/**
	 * @see stream.io.AbstractDataStream#readItem(stream.data.Data)
	 */
	@Override
	public Data readNext() throws Exception {

		Data instance = DataFactory.create();
		boolean success = false;

		while (!success) {

			try {
				while (streamReader == null) {
					OAuthRequest request = new OAuthRequest(Verb.GET,
							"https://stream.twitter.com/1.1/statuses/sample.json");

					authService.signRequest(accessToken, request);
					Response response = request.send();
					streamReader = new BufferedReader(new InputStreamReader(
							response.getStream()));
				}

				String line = streamReader.readLine();
				log.info("line: {}", line);
				if (line == null)
					return null;

				try {
					JSONParser parser = new JSONParser(
							JSONParser.DEFAULT_PERMISSIVE_MODE);
					JSONObject map = (JSONObject) parser.parse(line);
					Object val = map.get("id_str");
					if (val == null) {
						continue;
					}
					instance.put("@id", new Long(map.get("id_str") + ""));

					for (String key : map.keySet()) {
						try {
							instance.put(key, (Serializable) map.get(key));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					log.error("Error while parsing tweet from JSON-string: {}",
							e.getMessage());
					e.printStackTrace();
					instance.put("@rawTweet", line);
				}
				success = true;
			} catch (Exception e) {
				log.error("Error reading from stream: {}", e.getMessage());
				e.printStackTrace();
				streamReader = null;
			}
		}
		return instance;
	}
}