/**
 * 
 */
package stream.twitter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.annotations.Parameter;

/**
 * <p>
 * A simple processor that posts messages to twitter. If the message (extracted
 * from an item by means of the <code>key</code> attribute) is longer than 140
 * characters, it will be partitioned into multiple postings.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class SendTweet extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(SendTweet.class);
	protected OAuthService authService;

	String apiKey = null;
	String apiSecret = null;
	String token = null;
	String tokenSecret = null;

	String key = "text";

	protected Token accessToken;

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {

		super.init(ctx);

		log.debug("Instantiating authService...");
		authService = new ServiceBuilder().provider(TwitterApi.class)
				.apiKey(apiKey).apiSecret(apiSecret).build();

		log.debug("using apiKey      '{}'", apiKey);
		log.debug("using apiSecret   '{}'", apiSecret);
		log.debug("using token       '{}'", token);
		log.debug("using tokenSecret '{}'", tokenSecret);

		if (token != null && tokenSecret != null) {
			log.info("Found access-token with token: {}, tokenSecret: {}",
					token, tokenSecret);
			accessToken = new Token(token, tokenSecret);
		} else
			log.info("No access-token specified!");

		if (accessToken == null) {

			Token token = authService.getRequestToken();
			String authUrl = authService.getAuthorizationUrl(token);
			log.info("Auth URL: {}", authUrl);
			System.out.println("Authorization required!");
			System.out
					.println(" (1) Open the following authentication URL in your browser: "
							+ authUrl);
			System.out
					.print(" (2) Type in the PIN code obtained from the URL: ");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			String pin = reader.readLine();
			reader.close();

			Verifier verifier = new Verifier(pin);
			accessToken = authService.getAccessToken(token, verifier);
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (input.get(key) == null) {
			return input;
		}

		try {

			final List<String> messages = new ArrayList<String>();

			String text = input.get(key).toString();
			if (text.length() + 1 > 140) {
				log.debug("Text exceeds 140 characters and needs splitting!");

				final List<String> words = new ArrayList<String>();
				for (String w : text.split(" ")) {
					words.add(w);
				}

				StringBuffer current = new StringBuffer();

				Iterator<String> it = words.iterator();
				while (it.hasNext()) {
					String word = it.next();

					if (current.length() + (1 + word.length()) > 140) {
						messages.add(current.toString());
						log.debug("Adding fragment '{}'", current.toString());
						current = new StringBuffer();
					}
					current.append(word + " ");
				}

				if (!current.toString().trim().isEmpty()) {
					messages.add(current.toString());
				}

			} else {
				messages.add(text);
			}

			for (String message : messages) {
				post(message);
			}

		} catch (Exception e) {
			log.error("Failed to POST status update: {}", e.getMessage());
			e.printStackTrace();
		}
		return input;
	}

	protected void post(String msg) throws Exception {
		try {
			OAuthRequest request = new OAuthRequest(Verb.POST,
					"https://api.twitter.com/1.1/statuses/update.json");

			request.addQuerystringParameter("status", msg);
			authService.signRequest(accessToken, request);

			Response res = request.send();
			log.debug("Response:\n{}", res);
			if (res.isSuccessful()) {
				log.debug("request was successful!");
			} else {
				log.warn("request produced error!");
			}
			log.debug("response.body:\n{}", res.getBody());
		} catch (Exception e) {
			log.error("Failed to POST status update: {}", e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey
	 *            the apiKey to set
	 */
	@Parameter(description = "The api key, required for OAuth authentication.", required = true)
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}

	/**
	 * @param apiSecret
	 *            the apiSecret to set
	 */
	@Parameter(description = "The api key secret, required for OAuth authentication.", required = true)
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	@Parameter(description = "The access token, required for OAuth authentication.", required = true)
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenSecret
	 */
	public String getTokenSecret() {
		return tokenSecret;
	}

	/**
	 * @param tokenSecret
	 *            the tokenSecret to set
	 */
	@Parameter(description = "The access token secret, required for OAuth authentication.", required = true)
	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	@Parameter(description = "The key from which the text for the tweet is being extracted. Default is `text`.")
	public void setKey(String key) {
		this.key = key;
	}
}
