/**
 * 
 */
package stream.text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class GuessLanguage extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(GuessLanguage.class);
	String key = "text";
	String label = "@lang";
	Map<String, Set<String>> wordLists = new HashMap<String, Set<String>>();

	public GuessLanguage() {

		for (String l : LanguageScore.LANGUAGES) {

			String lang = l.toLowerCase();
			URL url = GuessLanguage.class.getResource("/stop-words-" + lang
					+ ".txt");
			log.info("Reading stop-words from {}", url);

			try {

				Set<String> words = new TreeSet<String>();
				BufferedReader r = new BufferedReader(new InputStreamReader(
						url.openStream()));
				String line = r.readLine();
				while (line != null) {

					line = line.trim();
					int idx = line.indexOf(" ");
					if (idx > 0)
						line = line.substring(0, idx);

					words.add(line.trim());
					line = r.readLine();
				}

				log.info("Read {} stopword for language '{}'", words.size(),
						lang);
				wordLists.put(lang, words);

			} catch (Exception e) {
				log.error("Failed to read stop-word list for language {}: {}",
						lang, e.getMessage());
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);

		if (value == null)
			return input;

		String text = value.toString();
		String lang = guessLanguage(text);
		if (lang != null) {
			input.put(label, lang);
		}

		return input;
	}

	public SortedSet<LanguageScore> score(String text) {

		Set<String> words = new HashSet<String>();
		for (String w : text.split("\\s+")) {
			words.add(w.trim());
		}

		return score(words);
	}

	public SortedSet<LanguageScore> score(Set<String> words) {

		// log.info( "Checking {} words...", words.size() );

		SortedSet<LanguageScore> scores = new TreeSet<LanguageScore>();

		for (String lang : this.wordLists.keySet()) {
			Set<String> stopWords = this.wordLists.get(lang);
			Integer count = 0;
			// log.info( "  Checking for language {} using {} stopwords", lang,
			// stopWords.size() );
			for (String word : words) {
				if (stopWords.contains(word.trim()))
					count++;
			}

			scores.add(new LanguageScore(lang, new Double(count)));
		}

		return scores;
	}

	public String guessLanguage(String text) {
		return this.score(text).first().getLanguage();
	}
}
