/**
 * 
 */
package stream.text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class StopWordFilter implements WordFilter {

	static Logger log = LoggerFactory.getLogger(StopWordFilter.class);

	Set<String> stopWords = new HashSet<String>();

	String[] stopLists = new String[] { "/text/stop-words-en.txt",
			// "/text/stop-words-it.txt", "/text/stop-words-es.txt",
			"/text/stop-words-de.txt" };

	public StopWordFilter() {
		try {

			for (String stopList : stopLists) {

				URL url = StopWordFilter.class.getResource(stopList);

				if (url == null)
					url = StopWordFilter.class
							.getResource("/text/stop-words-en.txt");

				log.info("Loading stop-words from " + url.toString());

				BufferedReader r = new BufferedReader(new InputStreamReader(
						url.openStream()));
				String line = r.readLine();
				while (line != null) {
					stopWords.add(line.trim().toLowerCase());
					line = r.readLine();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			stopWords = new HashSet<String>();
		}

		log.info("StopWordFilter contains " + stopWords.size() + " stop words.");
	}

	/**
	 * @see edu.udo.cs.kilab.twitter.text.filter.WordFilter#filter(java.lang.String)
	 */
	public String filter(String w) {
		if (w != null && stopWords.contains(w.trim().toLowerCase())) {
			log.trace("Word '{}' is a stop-word!", w);
			return null;
		}

		return w;
	}

	public boolean matches(String w) {
		return stopWords.contains(w.trim().toLowerCase());
	}
}