/**
 * 
 */
package stream.text;

import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.util.URLUtilities;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.Language;

/**
 * @author chris
 * 
 */
public class DetectLanguage extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(DetectLanguage.class);

	String key = "text";
	String label = "lang";

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		List<String> profiles = new ArrayList<String>();

		URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
		URL[] urls = cl.getURLs();
		for (URL url : urls) {
			log.debug("URL: {}", url);

			if (url.toString().startsWith("file:")) {

				if (url.toString().endsWith(".jar")) {
					File file = new File(url.getPath());
					if (file.getName().endsWith(".jar")) {
						log.debug("Scanning jar-file {}", file);
						JarFile jar = new JarFile(file);
						List<String> resources = findResources(jar,
								"profiles/", ".*");
						for (String resource : resources) {
							try {
								log.debug("Found profile: {} in {}", resource,
										file);
								URL resUrl = DetectLanguage.class
										.getResource("/" + resource);
								log.debug("   resource is at {}", resUrl);
								String prof = URLUtilities.readContent(resUrl);
								if (!prof.isEmpty())
									profiles.add(prof);
							} catch (Exception e) {

							}
						}
					}
				}
			}
		}

		DetectorFactory.loadProfile(profiles);
	}

	public static List<String> findResources(JarFile jar, String packageName,
			String nameRegex) throws ClassNotFoundException {
		List<String> classes = new ArrayList<String>();
		log.debug("Checking jar-file {}", jar.getName());
		Enumeration<JarEntry> en = jar.entries();
		while (en.hasMoreElements()) {

			JarEntry entry = en.nextElement();
			entry.getName();
			log.debug("Checking JarEntry '{}'", entry.getName());

			if (entry.getName().matches(nameRegex)
					&& !entry.getName().endsWith("/")
					&& entry.getName().startsWith(packageName)) {
				try {
					classes.add(entry.getName());
				} catch (NoClassDefFoundError ncdfe) {
				} catch (Exception e) {
					log.error("Failed to load class for entry '{}'",
							entry.getName());
				}
			}

		}

		return classes;
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value != null) {

			try {
				String txt = value.toString();
				Detector detector = DetectorFactory.create();
				detector.append(txt);
				log.debug("--------------------------------------------------------------------------------");
				log.debug("text: {}", txt);
				String detected = detectLanguage(txt);
				if (detected == null)
					detected = "?";
				input.put(label, detected);
				log.debug("lang: {}", detected);
				log.debug("");
				ArrayList<Language> langs = detector.getProbabilities();
				for (Language lang : langs) {
					log.debug("   prob({}) = {}", lang.lang, lang.prob);
				}
				log.debug("--------------------------------------------------------------------------------");
			} catch (Exception e) {
				log.error("Error: {}", e.getMessage());
			}
		}

		return input;
	}

	public String detectLanguage(String txt) throws Exception {
		Detector detector = DetectorFactory.create();
		detector.append(txt);
		log.debug("--------------------------------------------------------------------------------");
		log.debug("text: {}", txt);
		String detected = detector.detect();
		return detected;
	}
}
