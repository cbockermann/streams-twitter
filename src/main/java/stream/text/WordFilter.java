/**
 * 
 */
package stream.text;

/**
 * @author chris
 *
 */
public interface WordFilter {

	public abstract String filter(String w);

	public abstract boolean matches( String w );
}