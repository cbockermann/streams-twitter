/**
 * 
 */
package stream.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chris
 *
 */
public class MentionsFilter implements WordFilter {

	Pattern p = Pattern.compile( "@[\\w_\\-\\d]*" );
    
	/**
	 * @see edu.udo.cs.kilab.twitter.text.filter.WordFilter#filter(java.lang.String)
	 */
	public String filter(String w) {
	    
	    if( w.length() > 1 && w.charAt( 0 ) == '@' )
	        return w;
		
		Matcher m = p.matcher( w );
		if( m.find() )
			return w.substring( m.start(), m.end() );
		
		return null;
	}
	
	public boolean matches( String w ){
        
        if( w.length() > 1 && w.charAt(0) == '@' )
            return true;
        
        Matcher m = p.matcher( w );
	    return m.find(); 
	}
}