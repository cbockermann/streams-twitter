package stream.text;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 * <p>
 * This is a simple keyword filter. It contains a set of popular keywords and
 * checks whether a words is contained or not. The keyword check is case insensitive.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class KeywordFilter
    implements WordFilter
{
    Set<String> keywords = new HashSet<String>();

    HashTagFilter htf = new HashTagFilter();
    MentionsFilter mf = new MentionsFilter();
    UrlFilter uf = new UrlFilter();
    

    /**
     * Sets the set of keywords.
     * 
     * @param words The keywords to be used for filtering.
     */
    public void setKeywords( Collection<String> words ){
        keywords = new HashSet<String>();
        for( String word : words )
            keywords.add( word.toLowerCase() );
    }
    

    /**
     * @see edu.udo.cs.kilab.twitter.text.filter.WordFilter#filter(java.lang.String)
     */
    @Override
    public String filter(String w)
    {
        if( keywords.contains( w.toLowerCase() ) )
            return w.toLowerCase();

        if( htf.filter( w ) != null )
            return null;
        
        if( mf.filter( w ) != null )
            return null;
        
        if( uf.filter( w ) != null )
            return null;

        if( ! w.matches( "\\w+" ) )
            return null;
        
        return w;
    }
    
    public boolean matches( String w ){
        return keywords.contains( w.trim().toLowerCase() );
    }
}