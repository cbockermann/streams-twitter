package stream.text;

import java.util.LinkedList;
import java.util.List;

public class LanguageScore implements Comparable<LanguageScore> {
	static List<String> LANGUAGES = new LinkedList<String>();
	static {
		LANGUAGES.add("EN");
		LANGUAGES.add("DE");
		LANGUAGES.add("ES");
		LANGUAGES.add("IT");
		LANGUAGES.add("FI");
		LANGUAGES.add("HU");
		LANGUAGES.add("PT");
		LANGUAGES.add("NL");
		LANGUAGES.add("FR");
		LANGUAGES.add("CZ");
		LANGUAGES.add("TR");
		LANGUAGES.add("SE");
	}

	String language;
	Double value;

	public LanguageScore(String lang, Double val) {
		this.language = lang.toUpperCase();
		this.value = val;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o instanceof LanguageScore) {
			LanguageScore other = (LanguageScore) o;
			return this.language.equals(other.getLanguage());
		}

		return false;
	}

	@Override
	public int compareTo(LanguageScore o) {
		if (this.equals(o))
			return 0;

		int result = getValue().compareTo(o.getValue());
		if (result == 0) {
			Integer idx1 = LANGUAGES.indexOf(this.getLanguage().toUpperCase());
			Integer idx2 = LANGUAGES.indexOf(o.getLanguage().toUpperCase());
			return idx1.compareTo(idx2);
		}

		return -result;
	}

	public String toString() {
		return "[lang=" + language + ", score=" + value + "]";
	}
}