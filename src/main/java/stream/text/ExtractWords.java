package stream.text;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import stream.AbstractProcessor;
import stream.Data;

public class ExtractWords extends AbstractProcessor {

	KeywordFilter keywordFilter = new KeywordFilter();

	StopWordFilter stopwordFilter = new StopWordFilter();

	String key = "text";

	String prefix = "word:";

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value == null)
			return input;

		Map<String, Integer> counts = new LinkedHashMap<String, Integer>();

		for (String tok : value.toString().split("\\b+")) {

			tok = tok.trim();

			if (tok.matches("\\d+(\\.\\d+)?"))
				continue;

			String kw = stopwordFilter.filter(keywordFilter.filter(tok.trim()));
			if (kw != null) {

				String word = this.key + ":" + kw;
				if (prefix != null)
					word = prefix + this.key + ":" + kw;

				Integer count = counts.get(word);
				if (count == null) {
					counts.put(word, 1);
				} else {
					counts.put(word, count + 1);
				}
			}
		}

		for (String word : counts.keySet()) {
			input.put(word, counts.get(word));
		}
		return input;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}