/**
 * 
 */
package stream.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chris
 *
 */
public class HashTagFilter implements WordFilter {

	Pattern p = Pattern.compile( "#\\w+" );
	
	/**
	 * @see edu.udo.cs.kilab.twitter.text.filter.WordFilter#filter(java.lang.String)
	 */
	public String filter(String w) {
	    
	    if( w.length() > 1 && w.charAt( 0 ) == '#' )
	        return w;
	    
		Matcher m = p.matcher( w );
		if( m.find() ){
			
			// we try to not consider &#39... as a hash-tag
			//
			if( m.start() > 0 && w.charAt( m.start() - 1 ) == '&' )
				return null;
				
			return w.substring( m.start(), m.end() );
		}
		
		return null;
	}
	
	public boolean matches( String w ){
	    
	    if( w.length() > 1 && w.charAt(0) == '#' )
	        return true;
	    
	    Matcher m = p.matcher( w );
	    return m.find();
	}
}
