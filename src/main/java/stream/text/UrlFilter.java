/**
 * 
 */
package stream.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chris
 * 
 */
public class UrlFilter implements WordFilter {

	Pattern p = Pattern.compile("http://[\\w\\./\\-]+");

	/**
	 * @see edu.udo.cs.kilab.twitter.text.filter.WordFilter#filter(java.lang.String)
	 */
	public String filter(String w) {

		Matcher m = p.matcher(w);
		if (m.find())
			return w.substring(m.start(), m.end());

		return null;
	}

	public boolean matches(String w) {
		Matcher m = p.matcher(w);
		return (m.find());
	}
}