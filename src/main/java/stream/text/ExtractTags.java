/**
 * 
 */
package stream.text;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class ExtractTags extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(ExtractTags.class);

	String key = "text";

	String into = "tags";

	boolean lowercase = true;

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value == null)
			return input;

		String msg = value.toString();
		String[] tags = extract(msg);
		if (tags != null) {
			input.put(into, tags);
		}

		return input;
	}

	public static String[] extract(String msg) {
		Set<String> tags = new HashSet<String>();
		for (String tok : msg.split("[^#\\w]+")) {
			if (tok.startsWith("#") && tok.length() > 1) {
				tags.add(tok.trim().toLowerCase());
			}
		}
		if (!tags.isEmpty()) {
			log.info("Found {} tags: {}", tags.size(), tags);
			String[] hashtags = tags.toArray(new String[tags.size()]);
			return hashtags;
		}
		return null;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the into
	 */
	public String getInto() {
		return into;
	}

	/**
	 * @param into
	 *            the into to set
	 */
	public void setInto(String into) {
		this.into = into;
	}

	/**
	 * @return the lowercase
	 */
	public boolean isLowercase() {
		return lowercase;
	}

	/**
	 * @param lowercase
	 *            the lowercase to set
	 */
	public void setLowercase(boolean lowercase) {
		this.lowercase = lowercase;
	}
}
