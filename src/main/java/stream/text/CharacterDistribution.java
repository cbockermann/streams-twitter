/**
 * 
 */
package stream.text;

import java.io.Serializable;

/**
 * @author chris
 * 
 */
public class CharacterDistribution implements Serializable {

	/** The unique class ID */
	private static final long serialVersionUID = -2420385072602631978L;
	Long total = 0L;
	Double[] frequencies = new Double[Character.MAX_VALUE];
	int min = -1;
	int max = -1;
	int nonZero = 0;

	public CharacterDistribution() {
	}

	public Double p(Character c) {

		char idx = c.charValue();
		if (frequencies[idx] == null)
			return 0.0d;

		return frequencies[idx] / total.doubleValue();
	}

	public Double p(String str) {
		Double prob = 1.0d;

		for (char c : str.toCharArray()) {
			prob *= p(c);
		}

		return prob;
	}

	public void add(Character c) {
		char idx = c.charValue();
		if (frequencies[idx] == null) {
			if (min < 0 || idx < min) {
				min = idx;
			}

			if (max < 0 || max < idx) {
				max = idx;
			}
			nonZero++;
			frequencies[idx] = new Double(1.0d);
		} else {
			frequencies[idx] += 1.0;
		}
		total++;
	}

	public CharacterDistribution(String text) {
		for (int i = 0; i < text.length(); i++) {
			Character c = text.charAt(i);
			add(c);
		}
	}

	public Double[] asDoubleArray() {
		Double[] array = new Double[frequencies.length];
		for (int i = 0; i < frequencies.length; i++) {
			if (frequencies[i] != null) {
				array[i] = frequencies[i] / total.doubleValue();
			}
		}
		return array;
	}

	public int getMinIndex() {
		return min;
	}

	public int getMaxIndex() {
		return max;
	}

	public int numberOfNonZeroes() {
		return nonZero;
	}

	public Double count(Character c) {
		char idx = c.charValue();
		if (frequencies[idx] == null)
			return 0.0d;

		return frequencies[idx];
	}

	public void weight(char c, Double d) {
		if (frequencies[c] != null) {
			frequencies[c] *= d;
		}
	}
}