/**
 * 
 */
package stream.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;

/**
 * @author chris
 * 
 */
public class TwitterStream extends AbstractTwitterStream {

	static Logger log = LoggerFactory.getLogger(TwitterStream.class);

	String[] keywords;

	public TwitterStream() {
	}

	public TwitterStream(URL url) {
		this.twitterUrl = url.toString();
	}

	/**
	 * @return the keywords
	 */
	public String[] getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords
	 *            the keywords to set
	 */
	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}

	/**
	 * @see stream.io.AbstractDataStream#readItem(stream.data.Data)
	 */
	@Override
	public Data readNext() throws Exception {

		Data instance = DataFactory.create();
		boolean success = false;

		while (!success) {

			try {
				while (streamReader == null) {
					OAuthRequest request = new OAuthRequest(Verb.POST,
							"https://stream.twitter.com/1.1/statuses/filter.json");

					int conditions = 0;

					if (keywords != null) {
						String track = join(keywords);
						log.info("Adding keywords {}, filter:   track={}",
								keywords, track);
						request.addBodyParameter("track", track);
						conditions++;
					} else {
						log.info("No keywords specified...");
					}

					if (conditions < 1) {
						log.error("No filter criteria specified for stream! Use parameter 'keywords' to specify keywords!");
						return null;
					}

					authService.signRequest(accessToken, request);
					Response response = request.send();
					streamReader = new BufferedReader(new InputStreamReader(
							response.getStream()));
				}

				String line = streamReader.readLine();
				if (line == null)
					return null;

				try {
				    log.debug( "line: {}", line);
					JSONParser parser = new JSONParser(
							JSONParser.DEFAULT_PERMISSIVE_MODE);
					JSONObject map = (JSONObject) parser.parse(line);
					instance.put("@id", new Long(map.get("id_str") + ""));

					for (String key : map.keySet()) {
						try {
							instance.put(key, (Serializable) map.get(key));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					log.error("Error while parsing tweet from JSON-string: {}",
							e.getMessage());
					e.printStackTrace();
				}
				instance.put("@rawTweet", line);
				success = true;
			} catch (Exception e) {
				log.error("Error reading from stream: {}", e.getMessage());
				e.printStackTrace();
				streamReader = null;
			}
		}
		return instance;
	}
}