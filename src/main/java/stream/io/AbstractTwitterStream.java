/**
 * 
 */
package stream.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 * @author chris
 * 
 */
public abstract class AbstractTwitterStream extends AbstractStream {

	protected OAuthService authService;
	protected String twitterUrl;
	String apiKey = null;
	String apiSecret = null;
	String token = null;
	String tokenSecret = null;
	protected Token accessToken;
	protected BufferedReader streamReader;

	public static final String join(String[] keys) {

		if (keys == null)
			return null;

		StringBuffer s = new StringBuffer();

		for (int cnt = 0; cnt < keys.length; cnt++) {
			String key = keys[cnt];
			if (!key.trim().isEmpty()) {
				s.append(key.trim());

				if (cnt + 1 < keys.length)
					s.append(",");
			}
		}

		return s.toString();
	}

	/**
	 * @param url
	 */
	public AbstractTwitterStream(SourceURL url) {
		super(url);
	}

	/**
	 * @param in
	 */
	public AbstractTwitterStream(InputStream in) {
		super(in);
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey
	 *            the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}

	/**
	 * @param apiSecret
	 *            the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	public void setConsumerKey(String key) {
		this.setApiKey(key);
	}

	public String getConsumerKey() {
		return getApiKey();
	}

	public void setConsumerSecret(String key) {
		this.setApiSecret(key);
	}

	public String getConsumerSecret() {
		return getApiSecret();
	}

	public void setAccessToken(String key) {
		this.setToken(key);
	}

	public String getAccessToken() {
		return getToken();
	}

	public void setAccessTokenSecret(String sec) {
		this.setTokenSecret(sec);
	}

	public String getAccessTokenSecret() {
		return getTokenSecret();
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenSecret
	 */
	public String getTokenSecret() {
		return tokenSecret;
	}

	/**
	 * @param tokenSecret
	 *            the tokenSecret to set
	 */
	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	/**
	 * @see stream.io.AbstractDataStream#init()
	 */
	@Override
	public void init() throws Exception {
		super.init();

		log.info("Creating authService, apiKey: {}, apiSecret: {}", apiKey,
				apiSecret);

		authService = new ServiceBuilder().provider(TwitterApi.class)
				.apiKey(apiKey).apiSecret(apiSecret).build();

		if (token != null && tokenSecret != null) {
			log.info("Found access-token with token: {}, tokenSecret: {}",
					token, tokenSecret);
			accessToken = new Token(token, tokenSecret);
		} else
			log.info("No access-token specified!");

		if (accessToken == null) {

			Token token = authService.getRequestToken();
			String authUrl = authService.getAuthorizationUrl(token);
			System.out.println("Authorization required!");
			System.out
					.println(" (1) Open the following authentication URL in your browser: "
							+ authUrl);
			System.out
					.print(" (2) Type in the PIN code obtained from the URL: ");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			String pin = reader.readLine();
			reader.close();

			Verifier verifier = new Verifier(pin);
			accessToken = authService.getAccessToken(token, verifier);
		}
	}

	/**
	 * @see stream.io.DataStream#close()
	 */
	@Override
	public void close() throws Exception {
	}

	/**
	 * 
	 */
	public AbstractTwitterStream() {
		super();
	}

}