ExtractTags
===========

This processor extracts hash-tags from twitter messages. Hash-tags
start with a `#` character, followed from one or more alpha-numeric
characters.

By default, the tags extracted are stored in an array that will be
added to the data items under key `tags`. Tags are also converted
to lowercase strings. The twitter message is expected to be found in
key `text`.

These default settings can be changed using the parameters `key` for
the key that contains the twitter message, the parameter `into` to
set the name of the array in the data item and `lowercase`, which is
a boolean parameter specifying whether the tags shall be converted to
lowercase or left untouched.
