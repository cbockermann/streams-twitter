DetectLanguage
==============

This processors uses character distributions to detect the language
of a (short) text. The text is extracted from the key `text` from the
data items. The predicted language is put into the data item as key
`lang`.

The parameters `key` and `label` allow for changing the text attribute
to detect from and the predicted language attribute accordingly.

### Example

The following example shows a `DetectLanguage` processor, that detects
the language of a string found in attribute `message` and stores the
result in attribute `@language`:

      &lt;stream.twitter.text.DetectLanguage key="message"
                                             label="@language" /&gt;
