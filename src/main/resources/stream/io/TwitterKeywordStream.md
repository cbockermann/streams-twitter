This class implements a stream that connects to the twitter streaming API. It
uses the `filter` REST call to place a list of keywords (defined by the `keywords`
parameter) and opens a twitter stream that yields the tweets matching one of
those keywords.

For authentication, the implementation requires the `apiKey`, `apiSecret`,
`token` and `tokenSecret` parameters to be set. These can be obtained by
logging into the twitter web interface and creating a token within the developer
interface.
